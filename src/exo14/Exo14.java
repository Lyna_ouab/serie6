package exo14;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exo14 {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("one","two","three","four",
				"five","six","seven","eight","nine","ten",
				"eleven","twelve");
		Stream<String> wordsStream = strings.stream();
		System.out.println("The content of wordsStream a Stream of String;");
		wordsStream.forEach(System.out::println);
		
		Stream<String> stringsToUpper = strings.stream()
				.map(String::toUpperCase);
		System.out.println("\nThe content the Stream after toUpperCase");
		stringsToUpper.forEach(System.out::println);
		
		Stream<String> firstCharToUpper = strings.stream()
				.map(s -> ("" + s.charAt(0)))
				.map(s -> s.toUpperCase())
				.distinct();
		System.out.println("\nThe first Char only");
		firstCharToUpper.forEach(System.out::println);
		
		Stream<Integer> lengthOfString = strings.stream()
				.map(String::length)
				.distinct();
		System.out.println("\nLength of each String");
		lengthOfString.forEach(System.out::println);
		
		long count = strings.stream()
				.count();
		System.out.println("\nNombre of Strings in the Stream = " + count);
		
		long countPair = strings.stream()
				.map(String::length)
				.filter(length -> length % 2 == 0)
				.count();
		System.out.println("\nNombre of Strings with an even length = " + countPair);
		
		long maxLength = strings.stream()
				.map(String::length)
				.max(Integer::compare)
				.get();
		System.out.println("\nLength of the biggest String : " + maxLength);
		
		List<String> listOfOddLength = strings.stream()
				.map(String::toUpperCase)
				.filter(s -> s.length() % 2 != 0)
				.collect(Collectors.toList());
		System.out.println("\nList of Strings that have odd length:");
		listOfOddLength.forEach(System.out::println);
		
		
		String stringOfFiveChar = strings.stream()
				.filter(s -> s.length() <= 5)
				.sorted()
				.collect(Collectors.joining(", ", "{","}"));
		System.out.println("\nThe giant String : " + stringOfFiveChar);
		
		Map<Integer, List<String>> mapLength = strings.stream()
				.collect(
						Collectors.groupingBy(
								String::length,
								Collectors.toList()
						));
		System.out.println("\nThe map with List of String as values:");
		mapLength.forEach((key , value) -> 
				System.out.println("◄ key = " + key + ", value =" + value));
		
		Map<Character , String> mapFirstChar = strings.stream()
				.collect(
						Collectors.groupingBy(
								s -> s.charAt(0),
								Collectors.joining(", ")
								));
		System.out.println("\nThe map with String as values:");
		mapFirstChar.forEach((key , value) -> 
				System.out.println("► key = \"" + key + "\" , value =" + value));
	
	}
    
}
